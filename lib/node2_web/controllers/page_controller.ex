defmodule Node2Web.PageController do
  use Node2Web, :controller

  alias Node2.Chats

  def index(conn, _params) do
    messages =
      Chats.fetch_ordered(:inserted_at)
      |> Enum.map(fn message -> check_url(message.body) end)
    render conn, "index.html", messages: messages
  end

  defp check_url(url) do
    case URI.parse(url) do
      %URI{scheme: nil} -> {:text, url}
      %URI{host: nil} -> {:text, url}
      _ -> {:url, url}
    end
  end
end
